var express = require('express'),
  cors = require('cors')
  app = express(),
  port = process.env.PORT || 3000;

  app.use(cors())
  app.use(express.static(__dirname));

  var requestjson = require('request-json');

  //variable para poder usar el request-json
  var requestJson = require('request-json');
  //variable para que se pueda parsear el req.body y no salga como undefined
  var bodyParser = require('body-parser');
  app.use(bodyParser.json()); // for parsing application/json


  var path = require('path');

// Datos de mongodb
var mongoClient = require ('mongodb').MongoClient;

var url = "mongodb://localhost:27017/local"


var urlsegurosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/seguros?apiKey=foY_VFeCH3fghOv4gW95qeSePv6kKEJf";

//// Datos de Postgre
var pg = require ('pg');
var urlUsuarios = "postgres://docker:docker@localhost:5433/dbseguridad";


app.listen(port);




//lista movimientos de tarjetas
app.post('/tarmov', function(req, res) {
   mongoClient.connect(url, function(err,db){
       if(err)
       {
         console.log(err);
       }
       else
       {
         var col = db.collection('tarjetasmov');
        col.find({"usuario":req.body.usuario, "pan":req.body.pan}).limit(99).toArray(function(err, docs){
           res.send(docs);
         });
         db.close();
       }
   })
});

//lista movimientos de cuentas mongo
app.post('/cumov', function(req, res) {
   mongoClient.connect(url, function(err,db){

       if(err)
       {
         console.log(err);
       }
       else
       {
         var col = db.collection('cuentasmov');
          col.find({"usuario":req.body.usuario, "cuenta":req.body.cuenta}).limit(99).toArray(function(err, docs){
           res.send(docs);

         });
         db.close();
       }
   })
});


//lista de tarjetas mongo
app.post('/tarjetas', function(req, res) {
   mongoClient.connect(url, function(err,db){
       if(err)
       {
         console.log(err);
       }
       else
       {
         var col = db.collection('tarjetas');
        col.find({"usuario":req.body.usuario}).limit(99).toArray(function(err, docs){
           res.send(docs);

         });
         db.close();
       }
   })
});




//lista de cuentas mongo
app.post('/cuentas', function(req, res) {
 //res.sendFile(path.join(__dirname, 'index.html'));
   mongoClient.connect(url, function(err,db){
       if(err)
       {
         console.log(err);
       }
       else
       {
         var col = db.collection('cuentas');
      col.find({"usuario":req.body.usuario}).limit(99).toArray(function(err, docs){
           res.send(docs);
         });
         db.close();
       }
   })
});



///consulta de usuario Postgre
app.post('/usuario', function (req, res){
var clientePostgre= new pg.Client(urlUsuarios);
clientePostgre.connect();
const query = clientePostgre.query('select * from usuarios where usuario =$1;'
,[req.body.usuario],
(err,result) => {
if (err) {
  res.send(err);
}
else {
  res.send(result.rows);

}
});
} );



//Mongo Alta de cuentas
app.post('/altacu', function(req, res){
 mongoClient.connect(url, function(err,db){
   var col = db.collection('cuentas');
   db.collection('cuentas').insertOne(req.body, function(err, r) {
      });
   db.close();
   res.send('{"mensaje": "Cuenta dado de alta correctamente", "panel":true}');
 });
});

//Mongo Alta de Tarjetas
app.post('/altatar', function(req, res){
 mongoClient.connect(url, function(err,db){
   var col = db.collection('cuentas');
   db.collection('tarjetas').insertOne(req.body, function(err, r) {
   });
   db.close();
   res.send('{"mensaje": "Tarjeta dada de alta correctamente", "panel":true}');

 });
});


///Postgre Alta de usuario
app.post('/alta', function (req, res){
var clientePostgre= new pg.Client(urlUsuarios);
clientePostgre.connect();
const query = clientePostgre.query('insert into usuarios values ($1,$2,$3,$4,$5);'
,[req.body.nombre,req.body.apellidos,req.body.email,req.body.usuario, req.body.password],
(err,result) => {
if (err) {

  res.send(err);
}
else {
  res.send('{"mensaje": "Cliente dado de alta correctamente", "panel":true}');

}

});
} );

/// Postgre cambio de contraseña
app.post('/cambio', function (req, res){
var clientePostgre= new pg.Client(urlUsuarios);
clientePostgre.connect();
const query = clientePostgre.query('update usuarios set password =$1 where usuario=$2;'
,[req.body.password, req.body.usuario],

(err,result) => {
if (err) {

  res.send(err);
}
else {
  res.send('{"mensaje": "Contraseña cambiada", "panel":true}');

}

});
} );



///Postgre login de usuario
app.post('/login', function (req, res){
//Crear cliente Postgre

var clientePostgre= new pg.Client(urlUsuarios);
var tontada = "Tontada";
clientePostgre.connect();
const query = clientePostgre.query('select  count(*) from usuarios where usuario =$1 and password=$2;'
,[req.body.login,req.body.password],
(err,result) => {
if (err) {

  res.send(err);
}
else {

  if (result.rows[0].count >= 1)
  {
    res.send('{"mensaje": "Login correcto", "resultado":0}');

      }
      else{

      res.send('{"mensaje": "Usuario o Contraseña incorrectos", "resultado":1}');
      }

}

});
} );
