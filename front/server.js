var express = require('express'),
  app = express(),
  port = process.env.PORT || 2800;

  var path = require('path');

app.use(express.static(__dirname + '/mybank'));

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);



app.get('/', function (req, res){

  res.sendFile("index.html",{root:'.'}) ;
});
